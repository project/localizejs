
Module: Localizejs
Author: Eleonel Basili <http://drupal.org/u/eleonel>


Description
===========
Adds the Localize JS translation service to your Drupal website.

Requirements
============

* Localize js project key (https://localizejs.com).


Installation
============
Copy the 'localizejs' module directory in to your Drupal
sites/all/modules directory as usual.


Usage
=====
In the settings page enter your Localizejs Project Key.

All pages will now have the required JavaScript added to the
HTML header can confirm this by viewing the page source from
your browser.